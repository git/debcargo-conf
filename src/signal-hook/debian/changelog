rust-signal-hook (0.3.17-1~bpo12+pve1) proxmox-rust; urgency=medium

  * Rebuild for Debian Bookworm / Proxmox

 -- Proxmox Support Team <support@proxmox.com>  Wed, 24 Jul 2024 11:48:43 +0200

rust-signal-hook (0.3.17-1) unstable; urgency=medium

  * Team upload.
  * Package signal-hook 0.3.17 from crates.io using debcargo 2.6.0
  * Bump serial_test dev-dependency to 2.0.

 -- Peter Michael Green <plugwash@debian.org>  Fri, 17 Nov 2023 02:15:24 +0000

rust-signal-hook (0.3.15-1) unstable; urgency=medium

  * Team upload.
  * Package signal-hook 0.3.15 from crates.io using debcargo 2.6.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 29 Jun 2023 23:54:24 +0200

rust-signal-hook (0.3.14-1) unstable; urgency=medium

  * Team upload.
  * Package signal-hook 0.3.14 from crates.io using debcargo 2.5.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 01 Jun 2022 18:53:10 +0200

rust-signal-hook (0.3.13-2) unstable; urgency=medium

  * Team upload.
  * Package signal-hook 0.3.13 from crates.io using debcargo 2.5.0
  * Fix tests when run with no-default-features.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 20 Jan 2022 19:04:02 +0000

rust-signal-hook (0.3.13-1) unstable; urgency=medium

  * Team upload.
  * Package signal-hook 0.3.13 from crates.io using debcargo 2.5.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 30 Dec 2021 10:44:26 +0100

rust-signal-hook (0.1.13-3) unstable; urgency=medium

  * Team upload.
  * Package signal-hook 0.1.13 from crates.io using debcargo 2.5.0
  * Use a caret dependency for signal-hook-registry as upstream did in version
    0.1.17  (Closes: #1002149)

 -- Peter Michael Green <plugwash@debian.org>  Tue, 21 Dec 2021 19:58:06 +0000

rust-signal-hook (0.1.13-2) unstable; urgency=medium

  * Package signal-hook 0.1.13 from crates.io using debcargo 2.5.0
  * Disable the version sync tests (which aren't really very relavent
    in a downstream context) so the rest of the testsuite can run.
  * Apply a hunk from an upstream patch to fix error about renamed
    lint.

  [ Fabian Grünbichler ]
  * Team upload.
  * Disable tokio/futures related features, tokio 0.2 uses
    signal-hook-registry directly and tokio-signal is no more.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 02 Dec 2021 02:38:25 +0000

rust-signal-hook (0.1.13-1) unstable; urgency=medium

  * Team upload.
  * Package signal-hook 0.1.13 from crates.io using debcargo 2.4.0
  * Source upload (Closes: #953990)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 15 Mar 2020 15:36:32 +0100

rust-signal-hook (0.1.12-1) unstable; urgency=medium

  * Team upload.
  * Package signal-hook 0.1.12 from crates.io using debcargo 2.4.0

 -- Ximin Luo <infinity0@debian.org>  Tue, 24 Dec 2019 16:26:38 +0000

rust-signal-hook (0.1.7-2) unstable; urgency=medium

  * Team upload.
  * Package signal-hook 0.1.7 from crates.io using debcargo 2.2.10
  * Source upload for migration

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 06 Aug 2019 18:26:31 +0200

rust-signal-hook (0.1.7-1) unstable; urgency=medium

  * Package signal-hook 0.1.7 from crates.io using debcargo 2.2.10

 -- Julio Merino <julio@meroh.net>  Mon, 18 Feb 2019 18:34:43 +0100
